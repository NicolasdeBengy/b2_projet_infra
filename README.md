# B2_projet_infra

Le but du projet était de monter une infrastructure pour des bots de sniping de cryptomonnaies.

## Nos besoin

Une base de données, un serveur web pour une API accédant à la base de données et un reverse proxy.

## Notre solution

Un serveur par tâche (pour la performance).

- Base de données: Centos 7 avec PostgreSQL

- Serveur Web: Rocky Linux 8 avec une API faite avec Express.js

- Reverse Proxy: Rocky Linux 8 avec Nginx

## Installation

### Base de données

- Installation des packages

```bash
sudo dnf install https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
sudo dnf update -y
sudo dnf -qy module disable postgresql
sudo dnf install postgresql13 postgresql13-server firewalld
```

- Configuration de la base de données

```bash
sudo systemctl enable postgresql-13
sudo systemctl start postgresql-13

sudo adduser dba
sudo passwd dba
sudo nano /var/lib/pgsql/data/postgresql.conf
    # décommenter et modifier cette ligne pour qu'elle ressemblle à ça
    listen_addresses = 'addresse du serveur web'

sudo vi /var/lib/pgsql/data/pg_hba.conf
    # ajouter cette ligne à la fin
    host    dba             dba             addresseDuSrvWeb               md5

sudo systemctl restart postgresql

sudo -u dba psql
create database mydb;
create user myuser with encrypted password 'mypass';
grant all privileges on database mydb to myuser;
```

- Configuration de la sécu

```
sudo firewall-cmd --remove-service=cockpit --permanent
sudo firewall-cmd --add-service=postgresql --permanent
sudo firewall-cmd --add-port=5432 --permanent
sudo firewall-cmd --reload
```

### Serveur Web

- Installation des packages

```bash
sudo dnf module install nodejs:14
sudo dnf install git firewalld epel-release
sudo dnf install fail2ban fail2ban-firewalld -y
```

- Installation de l'API

```bash
sudo git clone https://github.com/NicolasdeBengy/RenduInfraApi
```

- Configuration de l'API

```bash
sudo vi ./RenduInfraApi/src/data.js
# Puis modifier les informations entre la 3ème et la 7ème ligne pour qu'elles correspondent à celle de votre base de données
```

- Création du service pour faire tourner l'API

```
sudo vi /etc/systemd/system/api.service
    [Unit]
    Description=Express API service
    After=network.target

    [Service]
    ExecStart=node chemin/vers/le/dossier/racine/de/l'api

    [Install]
    WantedBy=multi-user.target

sudo systemctl enable api.service
sudo systemctl start api.service
```

- Configuration de la sécu

```bash
sudo firewall-cmd --remove-service=cockpit --permanent
sudo firewall-cmd --add-port=8080 --permanent
sudo firewall-cmd --reload

sudo systemctl start fail2ban
sudo systemctl enable fail2ban
sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
sudo nano /etc/fail2ban/jail.local
    bantime = 1h
    findtime = 1h
    maxretry = 5
sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local
sudo systemctl restart fail2ban
sudo nano /etc/fail2ban/jail.d/sshd.local
    [sshd]
    enabled = true
    bantime = 1d
    maxretry = 3
sudo systemctl restart fail2ban
```

### Reverse Proxy

On a eu des problèmes pour la configuration de nginx en reverse proxy donc voilà un gif de chat pour un peu de corruption.

![ReverseProxy](gif/brokenReverseProxyCat.gif)
